/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.dualsoft.demo.DTO;

import net.dualsoft.demo.util.ResponseStatus;

/**
 *
 * @author nikola
 */
public class Response {
        public String message;
        public ResponseStatus status;
        public Object data;
        
        public Response(String message, ResponseStatus status, Object data){
            this.message = message;
            this.status = status;
            this.data = data;
        }
}
