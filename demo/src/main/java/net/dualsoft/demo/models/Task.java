/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.dualsoft.demo.models;

import java.time.LocalDateTime;
import java.util.UUID;
import net.dualsoft.demo.util.TaskState;

/**
 *
 * @author nikola
 */
public class Task {

    private UUID id;
    private String title;
    private String content;
    private TaskState state;
    private LocalDateTime dateCreated;
    private LocalDateTime dateStarted;
    private LocalDateTime dateDone;
    
    public Task(UUID id, String title, String content){
        this.id = id;
        this.title = title;
        this.content = content;
        this.state = TaskState.created;
    }
    
    public void setId(UUID id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setState(TaskState state) {
        this.state = state;
    }
    
    public void setDateCreated(LocalDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public void setDateStarted(LocalDateTime dateStarted) {
        this.dateStarted = dateStarted;
    }
    

    public void setDateDone(LocalDateTime dateDone) {
        this.dateDone = dateDone;
    }
    
    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public TaskState getState() {
        return state;
    }

    public LocalDateTime getDateCreated() {
        return dateCreated;
    }

    public LocalDateTime getDateDone() {
        return dateDone;
    }

    public LocalDateTime getDateStarted() {
        return dateStarted;
    }
    
}
