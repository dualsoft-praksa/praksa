/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.dualsoft.demo.models;

import java.util.HashMap;
import java.util.UUID;

/**
 *
 * @author nikola
 */
public class Todo {
    private UUID id;
    private String name;
    private String user;
    private HashMap<UUID, Task> tasks;
    
    public Todo(UUID id, String user){
        this.id = id;
        this.user = user;
        this.tasks = new HashMap<>();
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setTasks(HashMap<UUID, Task> tasks) {
        this.tasks = tasks;
    }

    public UUID getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public HashMap<UUID, Task> getTasks() {
        return tasks;
    }

    public void put(UUID randomUUID, Task task) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
