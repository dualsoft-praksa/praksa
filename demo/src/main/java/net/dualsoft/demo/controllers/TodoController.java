/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.dualsoft.demo.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.dualsoft.demo.models.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nikola
 */
@RestController
@RequestMapping("/todo")

public class TodoController {
    
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse response;
    
    private final HashMap<Long, Task> todos = new HashMap<>();
    private static Long idGen = 1L;
    
    @GetMapping("/{id}")
    public Task getTodo(@PathVariable Long id){
        Task todo = todos.get(id);
        if(todo == null){
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return todo;
        }
        return todo;
    }
    
    @GetMapping("/")
    public ArrayList<Task> getTodos(){
        ArrayList<Task> arr = new ArrayList<>(todos.values());
        return arr;
    }
    
    @GetMapping("/done/{id}")
    public Task doTodo(@PathVariable Long id){
        Task todo = todos.get(id);
        if(todo == null){
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return todo;
        }
        todo.setDone(true);
        todo.setDateDone(LocalDateTime.now());
        todos.replace(todo.getId(), todo);
        return todo;
    }
    
    @PostMapping("/add")
    public Task postTodo(@RequestBody Task todo){
  
        if(todo == null){
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return null;
        }
        if(todo.getTitle() == ""){
            todo.setTitle("Interesting title");
        }
        if(todo.getContent() == ""){
            todo.setContent("Interesting content");
        }
        todo.setDateCreated(LocalDateTime.now());
        todo.setDone(false);
        todo.setId(idGen++);
        todos.put(todo.getId(), todo);
        return todo;
    }
    
    @DeleteMapping("/delete/{id}")
    public Task deleteTodo(@PathVariable Long id){
        Task todo = todos.remove(id);
        if(todo == null){
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return todo;
        }
        return todo;
    }
   
}
