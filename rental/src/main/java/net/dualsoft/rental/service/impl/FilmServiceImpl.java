/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.dualsoft.rental.service.impl;

import java.util.List;
import net.dualsoft.rental.DAO.FilmDao;
import net.dualsoft.rental.entities.Film;
import net.dualsoft.rental.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author nikola
 */
@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmDao filmDao;
    
    @Override
    public Film get(Long id) {
       Film film = filmDao.getOne(id);
       return film;
    }

    @Override
    public List<Film> getAll() {
        List<Film> films = filmDao.findAll();
        return films;
    }

    @Override
    public Film add(Film film) {
        filmDao.save(film);
        return film;
    }
    
}
