/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.dualsoft.rental.service;

import java.util.List;
import net.dualsoft.rental.entities.Film;
import org.springframework.stereotype.Service;

/**
 *
 * @author nikola
 */
@Service
public interface FilmService {
    public Film get(Long id);
    public List<Film> getAll();
    public Film add(Film film);
}
