/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.dualsoft.rental.controller;

import java.util.List;
import net.dualsoft.rental.entities.Film;
import net.dualsoft.rental.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author nikola
 */
@RestController
@RequestMapping("/film")
public class FilmController {
    @Autowired
    private FilmService filmService;
    
    @GetMapping("/{id}")
    public Film getFilm(@PathVariable Long id){
        Film film = filmService.get(id);
        return film;
    }
    
    @GetMapping("/")
    public List<Film> getFilms(){
        List<Film> films = filmService.getAll();
        return films;
    }
    
    @PostMapping("/add")
    public Film addFilm(@RequestBody Film filmFromBody){
        Film film = filmService.add(filmFromBody);
        return film;
    }
}
